<?php
namespace Module\Core;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once(dirname(__FILE__).'/./App.conf.php');
use Module\Core as conf;
/**
 * Description of data_check
 *
 * @author moca
 */
class DataCheck {
    function __construct() 
    {
        $this->categoryLength   = conf\AppConf::CATEGORYS;
        $this->langLengt        = conf\AppConf::LANGS;
        $this->appLength        = conf\AppConf::APPLENGTH;
    }
    //put your code here
    function appNameCheck($name)
    {
        $n = htmlspecialchars($name);
        if(strlen($n) > $this->appLength){return FALSE;}
        if(strlen($n) < 2){return FALSE;}
        return $n;
    }
    
    function categoryNumCheck($category)
    {
        $c = intval($category);
        if($c === 0){return FALSE;}
        if($c > $this->categoryLength){return FALSE;}
        return $c;
    }
}
