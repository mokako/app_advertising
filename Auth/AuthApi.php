<?php
namespace Auth;
if(!isset($_SESSION)){ 
    session_start(); 
} 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
ini_set('log_errors','On');
ini_set('error_log','./log/php_error.log');
error_reporting(E_ALL ^ E_NOTICE);
*/
include_once(dirname(__FILE__).'/./Module/AuthClass.php');
include_once(dirname(__FILE__).'/./Module/AuthDataClass.php');
include_once(dirname(__FILE__).'/../Module/Core/DataCheck.php');
include_once(dirname(__FILE__).'/../Module/Core/App.conf.php');


use Auth\Module     as am;
use Module\Core     as dc;
/**
 * Description of auth_api
 *
 * @author moca
 */
class AuthApi 
{
    //put your code here
    function __construct()
    {
        $this->ac  =    new am\AuthClass();
        $this->db  =    new am\AuthDataClass($mode = 'json');
        $this->dc  =    new dc\DataCheck();
        $this->local = dc\AppConf::LOCALNAME;
    }
    public 
            function checkAuth($user_uq,$email)
            {
                return $this->db->authCheck($user_uq,$email);
            }
            //public function setMode($mode,$user_uq,$app,$item)
            function setMode($p)
            {
                //必ず最初にaccountの照合を行います。
                if(!$this->db->authCheck($p['user'],$_SESSION['user_email'])){$this->errorRespons('Request is invalid.');}
                error_log(var_export($p,TRUE));
                switch ($p['type'])
                {
                    case 'newAppBase':
                        /*
                         * $param2のuser_idを調べ次に$_SESSION['email']を調べて
                         * ２つとも揃っているなら$param3の名前のアプリのベースを登録します。
                         * 次に登録用コードを発行しそれを返します。
                         * 次からはこのコードを利用してアプリの情報を登録していきます。
                         */
                        if($this->dc->appNameCheck($p['an']) && $cn = $this->dc->categoryNumCheck($p['ct']))
                        {
                            if($this->db->createNewApp($p['user'],$p['an'],$cn))
                            {
                                $ary = ['message'=>['state'=>'Create a new app base.']];
                                $this->successRespons($ary);
                            }else{
                                $this->errorRespons("Failed to create a new application.");
                            }  
                        }else{
                            $this->errorRespons("Failed to create a new application.");
                        }
                        break;
                    case 'appList':
                        $result = $this->db->getAppList($p['user']);
                        if(count($result) != 0){
                            $ary = ["result"=>$result];
                            $this->successRespons($ary);
                        }else{
                            $this->errorRespons('The application is not yet.');
                        }
                        break;
                    case 'newAppLang':
                        $result = $this->db->setAppLangLink($p['user'],$p['app'],$p['an'],intval($p['item']));
                        if($result){
                            $ary = ['message'=>['state'=>'Create a new app link.']];
                            $this->successRespons($ary);
                        }else{
                            $this->errorRespons('Failed to create a new supporting language link.');
                        }
                        break;
                    case 'getAppLang':
                        /*user_idとapp_idを使ってアプリ情報を取得します。*/
                        $result = $this->db->getAppLangLink($p['user'],$p['app']);
                        if(count($result) != 0){
                            $ary = ['result'=>$result];
                            $this->successRespons($ary);
                        }else{
                            $this->errorRespons('Supporting language is not created yet.');
                        }
                        break;
                    default :
                        $this->errorRespons("Request is invalid. (type : ".$p['type'].")");
                        break;
                }
            }
            /* call back */
            function successRespons($code)
            {
                header("Content-Type: application/json; charset=utf-8");
                
                $status = true;
                $json = [
                    "status"=>$status,
                    "code"=>$code
                ];
                echo json_encode($json);
                exit();
            }
            function errorRespons($errorCode) 
            {
                header("Content-Type: application/json; charset=utf-8");
                $status = false;
                $result = null;
                $error = array(
                    "message"=>$errorCode 
                );
                $json = [
                    'status'=>$status,
                    'result'=>$result,
                    'error'=>$error
                ];
                echo json_encode($json);
                exit();
            }
            /* erro log */
            function errorLogging($message)
            {
                error_log($message,0);
            }
}
$ary = array(
    'type'      => FILTER_SANITIZE_ENCODED,
    'user'      => FILTER_SANITIZE_ENCODED,
    'app'       => FILTER_SANITIZE_ENCODED,
    'item'      => FILTER_SANITIZE_ENCODED,
    'an'        => FILTER_SANITIZE_ENCODED,
    'ct'        => FILTER_SANITIZE_NUMBER_INT
);
$item = filter_input_array(INPUT_POST,$ary);

$authView = new AuthApi();
$authView->setMode($item);