<?php
if(!isset($_SESSION)){ 
    session_start(); 
} 
include_once(dirname(__FILE__).'/./Module/AuthDataClass.php');
use \Auth\Module as ac;

$user = filter_input(INPUT_GET,'user_id',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ad = new ac\AuthDataClass();
if($ad->authCheck($user,$_SESSION['user_email']))
{
    include_once('View/Header.php');
    include_once('View/Contents.php');
    include_once('View/Footer.php');
}else{
    $_SESSION = array();
    if( ini_get( 'session.use_cookies' ) )
    {
        // セッション クッキーを削除
        $params = session_get_cookie_params();
        setcookie( session_name(), '', time() - 3600, $params[ 'path' ] );
    }
    session_destroy();
    header("Location:../index.php");
    exit();
}


