<?php
include_once('../Module/Core/App.conf.php');
use Module\Core as conf;
$local = conf\AppConf::LOCALNAME;
?>

<nav id="top-nav" class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarEexample1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="javascript:void(0);"><strong><i class="glyphicon glyphicon-dashboard"></i> Dashboard</strong></a>
        </div>

        <div class="collapse navbar-collapse" id="navbarEexample1">
            <ul class="nav navbar-nav">
                <li class="hidden-lg hidden-md hidden mode-set"><a class="text-right" href="javascript:void(0);"><i class="glyphicon glyphicon-list"></i> App settings</a></li>
                <li class="hidden-lg hidden-md hidden mode-set"><a class="text-right" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Description App</a></li>
                <li class="hidden-lg hidden-md hidden mode-set"><a class="text-right" href="javascript:void(0);"><i class="glyphicon glyphicon-link"></i> press release</a></li>
                <li class="hidden-lg hidden-md hidden mode-set"><a class="text-right" href="javascript:void(0);"><i class="glyphicon glyphicon-star"></i> Term & Privacy Policy</a></li>
            </ul>
            <p class="navbar-text navbar-right">  </p>
            <?php
                echo '<p class="navbar-text navbar-right text-right"><image src="'.$_SESSION['picture'].'" width="20" height="20" />  '.$_SESSION['name'].'   ';
                echo '<a class="navbar-link" href="http://'.$local.'/Auth/AuthHeader.php?login=3"><i class="glyphicon glyphicon-lock"></i> Logout   </a></p>';
                ?>
            
        </div>
    </div>
</nav>
<!-- /Header -->

<!-- Main -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 side-board">
            <!-- Left column -->
            <ul class="nav in side-board-list">
                <li class=""><a href="javascript:void(0);" class="custombtn" edit-type="newAppBaseField" ><i class="glyphicon glyphicon-plus"></i>  新規アプリベース</a></li>
                <li class="nav-header"><a href="javascript:void(0);" data-toggle="collapse" data-target="#user_app_list"><i class="glyphicon glyphicon-gift"></i>  アプリベース </a>
                    <ul class="nav nav-stacked collapse side-board-item" id="user_app_list">
                    </ul>
                </li>
            </ul>
            <ul class="nav nav-pills nav-stacked hidden mode-set">
                <li class="nav-header"></li>
                <li><a href="#"><i class="glyphicon glyphicon-list"></i> Layouts &amp; Templates</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-briefcase"></i> Toolbox</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-link"></i> Widgets</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Reports</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-book"></i> Pages</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-star"></i> Social Media</a></li>
            </ul>
        </div>
        <!-- /col-3 -->
        
        <!-- main contents -->
        <div class="col-lg-10 main-board">
            <div class="row">
                <!-- first panel -->
                <div class="col-md-12">
                    <div class="app-top">
                        <a href="javascript:void(0);" class="text-info hidden app-mode mode-lang mode-set">
                            <i class="glyphicon glyphicon-wrench"><strong id="app_name" app_id=""></strong></i>
                        </a>
                    </div>
                    <hr class="app-mode hidden mode-lang mode-set">
                    <div class="panel panel-warning app-mode mode-first">
                        <div class="panel-heading">
                            <h4>最初に<i class="glyphicon glyphicon-globe pull-right"></i></h4></div>
                        <div class="panel-body">
                            <p>アプリベースの作成は [ 新規アプリベース ] をアプリベースの編集は [ アプリベース ] を選択して下さい。</p>
                        </div>
                    </div>
                    <div class="panel panel-warning app-mode mode-first mode-desc">
                        <div class="panel-heading">
                            <h4>app-adv.io<i class="glyphicon glyphicon-globe pull-right"></i></h4></div>
                        <div class="panel-body">
                            <p>
                                当ウエブサービスはユーザーの方が作られたアプリの説明やプロモーション及び広告を簡単に作成する事を目的として作っております。<br>
                                せっかくアプリを作ったのは良いけど、アプリの説明や規約を一からウエブサイトで構築させるのはなかなか大変ですよね？<br>
                                それらを簡単に作成出来る場をこのサイトは提供出来ると思っています。
                            </p>
                        </div>
                    </div>
                    <div class="btn-group btn-group-justified hidden app-mode mode-desc mode-set">
                        <a href="javascript:void(0);" class="btn btn-primary col-sm-3">
                            <i class="glyphicon glyphicon-plus"></i>
                            <br> アプリの説明
                        </a>
                        <a href="javascript:void(0);" class="btn btn-primary col-sm-3">
                            <i class="glyphicon glyphicon-cloud"></i>
                            <br> 利用規約
                        </a>
                        <a href="javascript:void(0);" class="btn btn-primary col-sm-3">
                            <i class="glyphicon glyphicon-cog"></i>
                            <br> 個人情報保護方針
                        </a>
                        <a href="javascript:void(0);" class="btn btn-primary col-sm-3">
                            <i class="glyphicon glyphicon-question-sign"></i>
                            <br> プレスリリース
                        </a>
                    </div>
                    <hr class="hidden app-mode mode-desc mode-set">
                    <div class="panel panel-success hidden app-mode mode-base">
                        <div class="panel-heading">新規アプリベース<i class="glyphicon glyphicon-plus pull-right"></i></div>
                        <div class="panel-body">
                            <form class="form-horizontal col-sm-12">
                                <div class="form-group has-feedback" id="category-form">
                                    <label class="col-sm-2 control-label">カテゴリー:</label>
                                    <div class="col-sm-10">
                                        <select id="category" class="form-control" title="Apps category" name='category'>
                                        </select>
                                        <span class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">新規アプリベース名:</label>
                                    <div class="col-sm-10">  
                                        <input type="text" class="form-control"   edit-text="newAppBase" title="Set a new app base">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <button class="btn btn-success custombtn pull-right" edit-type="newAppBase" type="button">アプリベース作成</button>
                                </div>
                
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-default  hidden app-mode mode-lang">
                        <div class="panel-heading">サポート言語の追加<i class="glyphicon glyphicon-globe pull-right"></i></div>
                        <div class="panel-body">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">アプリ名</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="name2" class="form-control" edit-text="newAppLang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">対応言語</label>
                                    <div class="col-sm-10">
                                        <select id="lang" class="form-control" name='lang'>
                                        </select>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary pull-right custombtn" edit-type="newAppLang" type="button">CREATE</button>    
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <!--support lang list-->
                            <div class="col-md-12">
                                <ul class="list-group" id="langLink">
                                    
                                </ul>
                            </div>
                        </div>
                        <!--/panel-body-->
                    </div>
                    <!--/panel-->

                    <div class="well text-danger hidden app-mode mode-lang">アプリベースとそれに連なる言語別アプリ説明なども全て削除しますか？<a href="" class="btn-sm btn-danger pull-right">DELETE</a></div> 
                </div>
                
                <!--/second panel-->
                <!--set panel-->
                
                
                <!--/set panel-->
                
            </div>
            <!--/row-->
        </div>
        <!--/end main contents-->
    </div>
</div>
<!-- /Main -->

<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>

<div class="modal" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modal-title">Add Widget</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <p>Add a widget stuff here..</p>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->