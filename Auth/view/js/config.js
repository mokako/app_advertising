/* not jquery methods
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function browserLang()
{
    var ua = window.navigator.userAgent.toLowerCase();
    try 
    {
        // chrome
        if( ua.indexOf( 'chrome' ) !== -1 ){
            return ( navigator.languages[0] || navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,2);
        }
        // それ以外
        else{
            return ( navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,2);
        }
    }
    catch( e ) {
        return undefined;
    }
}

function getUrlVars()
{
    var vars = [], max = 0, hash = "", array = "";
    var url = window.location.search;

        //?を取り除くため、1から始める。複数のクエリ文字列に対応するため、&で区切る
    hash  = url.slice(1).split('&');    
    max = hash.length;
    for (var i = 0; i < max; i++) {
        array = hash[i].split('=');    //keyと値に分割。
        vars.push(array[0]);    //末尾にクエリ文字列のkeyを挿入。
        vars[array[0]] = array[1];    //先ほど確保したkeyに、値を代入。
    }
    return vars;
}


