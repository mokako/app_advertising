
$(document).ready(function(){
    var user_id = '';
    var app_id = '';
    var edit = false;
    (function(){
        var val = getUrlVars();
        if(val['user_id']){
            user_id = val['user_id'];
            setUsersApp(user_id);
            setCategory();
            setLang();
        }else{
             location.href = "../index.php?login=0";
        }
    })();
    $(".alert").addClass("in").fadeOut(4500);
//動的データ
    /* swap open/close side menu icons */
    $(".navigation-link").click(function(){
        
        
        
        
    });
    /* category select */
    $('[name=category]').change(function(){
        var cf = "#category-form";
        if($(this).val() === 'none'){
            $(cf).removeClass('has-success')
                    .addClass('has-warning')
                    .find('span.glyphicon')
                    .removeClass('glyphicon-ok')
                    .addClass('glyphicon-warning-sign');
        }else{
            $(cf).removeClass('has-warning')
                    .addClass('has-success')
                    .find('span.glyphicon')
                    .removeClass('glyphicon-warning-sign')
                    .addClass('glyphicon-ok');
        }
    });
    
    //custombtnはformデータを担当
    $(document).on('click','.custombtn',function(){
        var type = $(this).attr("edit-type");
        var obj;
        switch(type){
            case 'newAppBaseField':
                $(".app-mode").addClass("hidden");
                $(".mode-base").removeClass("hidden");
                break;
            case 'newAppBase':
                console.log(type);
                var text = $('[edit-text=newAppBase]').val();
                var val = $('[name=category]').val();
                if(text.length < 1 || text.length > 30 || val === 'none'){alert('error');return;}
                var obj = {
                    data:{type:'newAppBase', user:user_id, ct:val, an:text},
                    callback:function(obj){
                        if(obj.status){
                            setUsersApp(user_id);
                            $('[edit-text=newAppBase]').val('');
                            $('[name=category]').val('none');
                            modalSettings({title:'Success',message:obj.code.message.state,state:true});
                        }else{
                            modalSettings({title:'Unsuccess',message:obj.error.message,state:false});
                        }
                    },
                    error:function(obj){alert(obj.error.message);}
                };
                requestState(obj);
                break;
            case 'newAppLang':
                console.log(type);
                var text = $('[edit-text=newAppLang]').val();
                var val = $('[name=lang]').val();
                var i = {type:'newAppLang', user:user_id, app:app_id, an:text, item:val};
                console.log(i);
                if(text.length < 1 || text.length > 30 || val === 'none'){alert('Input value is invalid.');return;}
                var obj = {
                    data:{type:'newAppLang', user:user_id, app:app_id, an:text, item:val},
                    callback:function(obj){
                        if(obj.status){
                            $('[edit-text=newAppLang]').val('');
                            $('[name=lang]').val('0');
                            modalSettings({title:'Success',message:obj.code.message.state,state:true});
                        }else{
                            modalSettings({title:'Unsuccess',message:obj.error.message,state:false});
                        }
                    },
                    error:function(obj){alert(obj.error.message);}
                };
                requestState(obj);
                break;
            case 'appLangLink':
                app_id = $(this).attr("app_id");
                $(".app-mode").addClass("hidden");
                $(".mode-lang").removeClass("hidden");
                $("#app_name").text($(this).text());
                var obj = {
                    data:{type:'getAppLang', user:user_id, app:app_id},
                    callback:function(obj){
                        if(obj.status){
                            console.log(obj.code.result);
                            var items =[];
                            $.each(obj.code.result, function(key,val) {
                                items.push('<li class="list-group-item"><a href="javascript:type(0);" edit-type="appEdit" lang="'+val.lang+'">'+val.name+'  (  '+val.langIso+'  )</a></li>');
                            });
                            $("#langLink").html(items.join(''));
                        }else{
                            //modalSettings({title:'Unsuccess',message:obj.error.message,state:false});
                            console.log(obj.error.message);
                        }
                    },
                    error:function(obj){alert(obj.error.message);}
                };
                requestState(obj);
                break;
            default:
                break;
        }
    });
    //app_linkは取得したリンクの処理を担当します。formや表示等の変更
});

function setCategory()
{
    $.getJSON("./View/js/category.json" , function(data) {
        var obj = $('#category'),len = data.length;
        for(var i = 1; i <= len; i++) {
            obj.append($("<option>").attr({"value":i}).text(data[i - 1]));
        }
    });
    //$('#category')
}
function setLang()
{
    $.getJSON("./View/js/lang.json" , function(data) {
        var obj = $('#lang');
        //dataがサーバから受け取るjson値
        var count = 0;
        var items = [];
        $.each(data, function(key, val) {
          items.push('<option value='+count+' lang-date="'+key+'"><p class="text-center">'+ val.name +' :  '+ val.nativeName +'</p></option>');
          count++;
        });
        obj.html(items.join(''));
    });
}

function requestState(obj){
    $.ajax({
        method: "POST",
        url: "AuthApi.php",
        dataType:'json',
        data:obj.data
    })
    .done(function(result) {
        obj.callback(result);
    })
    .fail(function(obj) {
        obj.error(obj);
    });
}

function setUsersApp(user_id){
    //登録しているアプリを取得します。
    var obj = {
                data:{type:'appList', user:user_id},
                callback:function(obj){
                        if(obj.status){
                            //成功時はアプリへのリンクを作成し表示する。
                            //obj.code.resultに結果が入っています。
                            addElementsArrayFromData('#user_app_list',obj.code.result,'appList');
                        }else{
                        }
                    },
                error:null
            };
    requestState(obj);
}

function addElementsArrayFromData(element,itemArray,type){
    var value;
    var ary = $.map(itemArray,function(key,val){
        switch(type){
            case 'appList':
                return '<li><a href="javascript:void(0);" class="custombtn" edit-type="appLangLink" app_id="'+val+'"><strong><i class="glyphicon glyphicon-link"> '+key+'  </i></strong></a></li>';
                break;
            
            default:
                break;
        }
    });
    $(element).html(ary.join(''));
    //追加された要素をDOMに反映させます。
    console.log('access');
    $(document).ready(function(){
        var user_id = '';
        (function(){
            var val = getUrlVars();
            if(val['user_id']){
                user_id = val['user_id'];
            }else{
                 location.href = "../index.php?login=0";
            }
        })();
    });
}
/*
function appLink(user_id)
{
    $(".app_link").on('click',function(){
        var target = $(this).attr("app_target");
        switch(target){
            case 'app_id':
                $(".mode-first").addClass("hidden");
                $(".mode-second").removeClass("hidden");
                $("#app_name").text($(this).text()).attr();
                var app_id = $(this).attr("target_id");
                //app_id,user_id,ブラウザcountryCodeを読み取って反映
                var location = browserLang();
                $.ajax({
                        method: "POST",
                        url: "AuthApi.php",
                        dataType:'json',
                        data: { type:'appLink', user:user_id ,app:app_id, item:location}
                    })
                    .done(function(obj) {
                        if(obj.status){
                            //
                            addElementsArrayFromData('#app_lang_list',obj.code.result,'appList');
                        }else{
                            alert(obj.error.message);
                        }
                    })
                    .fail(function(obj) {
                       console.log('json error');
                    });
                    
                break;
            default:
                break;
        }   
    });
}
*/
function modalSettings(obj)
{
    var title = $('#modal-title');
    if(obj.state){
        title.removeClass('text-danger').addClass('text-success');
    }else{
        title.removeClass('text-success').addClass('text-danger');
    }
    title.text(obj.title);
    $('#modal-body').html('<p>'+obj.message+'</p>');
    $('.modal').modal('show');
}