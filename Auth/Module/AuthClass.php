<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Auth\Module;


include_once(__DIR__ .'/../../Module/Core/App.conf.php');
use Module\Core as conf;
/**
 * Description of authClass
 *
 * @author moca
 */
//該当のaccountが既にデータベースに作られているかみます。


class AuthClass {
    public $localName;
    function __construct() 
    {
        $this->localName = conf\AppConf::LOCALNAME;
    }
    public 
            function setGoogleAuth_2_0() 
            {
                define('CONSUMER_KEY', '632476337354-lcehjippog6oer4ads2rkj9chq0e1me2.apps.googleusercontent.com');
                define('CALLBACK_URL', 'http://'.$this->localName.'/Auth/Callback.php');
                // URL
                define('AUTH_URL', 'https://accounts.google.com/o/oauth2/auth');

                $scope = array(
                    'https://www.googleapis.com/auth/userinfo.profile', // 基本情報(名前とか画像とか)
                    'https://www.googleapis.com/auth/userinfo.email',   // メールアドレス
            );

//--------------------------------------
// 認証ページにリダイレクト
//--------------------------------------
        $params = array(
            'client_id' => CONSUMER_KEY,
            'redirect_uri' => CALLBACK_URL,
            'scope' => implode(' ', $scope),
            'response_type' => 'code',
        );
// リダイレクト
        header("Location: " . AUTH_URL . '?' . http_build_query($params));
    }
    
    
}
