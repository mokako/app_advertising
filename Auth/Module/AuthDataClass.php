<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Auth\Module;

include_once(__DIR__.'/../../Module/Core/DataClass.php');
use Module\Core as DB;
/**
 * Description of auth_data_class
 *
 * @author moca
 */
class AuthDataClass extends DB\DataClass{
    //put your code here
    function __construct($mode = "json") {
        parent::__construct($mode);
    }
    
    function setAuth($authName){
        
        //mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
        $sql = "INSERT INTO user_base_id (user_uq, email) SELECT ?, ? FROM (SELECT 1) AS dummy WHERE NOT EXISTS ( SELECT email FROM user_base_id WHERE email = ? ) LIMIT 1;";
        $stmt = $this->mysqli->prepare($sql);
        //mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
        $stmt->bind_param('sss',$str,$name,$name);
        $str  = $this->getHash();
        $name = $authName;
        $success = TRUE;
        $stmt->execute();
        $this->errorLogging($this->mysqli->error);
        $stmt->close();
        return $success;
    }
    function getAuthID($email){
        $sql = "SELECT user_uq FROM user_base_id WHERE email = ?;";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $stmt->bind_result($user_email);
        $stmt->fetch();
        $this->errorLogging($this->mysqli->error);
        $stmt->close();
        return $user_email;
    }
    /*
     * 新規アプリを登録します。登録用コードを同時に発行し今後それを使ってやりとりします。
     * アプリ名はuser_idと共に登録されるために重複しても大丈夫です。
     * ただし同じuser_idの同名のアプリは登録できません。
     **/
    function authCheck($user_uq,$user_email){
        $sql = "SELECT user_id FROM user_base_id WHERE email = ? AND user_uq = ?;";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ss',$user_email,$user_uq);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($user_id);
        $ary =[];
        while ($stmt->fetch()) {
            array_push($ary, $user_id);
        }
        $stmt->close();
        return count($ary) == 0 ? FALSE : TRUE;
        //return TRUE;
   }    
    function createNewApp($user_uq,$appName,$category){
        //mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
        $sql = "INSERT INTO app_base_id (app_uq, user_uq, name, cate) VALUES(?, ?, ?, ?);";
        $stmt = $this->mysqli->prepare($sql);
        $id_code  = $this->getHash();
        $stmt->bind_param('sssi',$id_code,$user_uq,$appName,$category);
        $success = $stmt->execute();
        $this->errorLogging($this->mysqli->error);
        $stmt->close();
        return $success;
    }
    function getAppList($user_uq){
        $sql = "SELECT app_uq,name FROM app_base_id WHERE user_uq = ?;";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s',$user_uq);
        $stmt->execute();
        //store_resultでバッファのデータを保存
        $stmt->store_result();
        //bind_resultで結果をセットする変数を設定
        $stmt->bind_result($app_uq, $app_name);
        $ary =[];
        while ($stmt->fetch()) {
            $ary[$app_uq] = $app_name;
        }
        $this->errorLogging($this->mysqli->error);
        $stmt->close();
        return $ary;
    }
    function setAppLangLink($user_uq,$app_id,$name,$lang)
    {
        
        $success = TRUE;
        //まずapp_base_idを参照しidを取得し次にidとlangを比較し在るならerroとなります.
        $sql = "INSERT INTO app_name (id,name,lang) SELECT target_id,?,? "
                ."FROM(SELECT id as target_id FROM app_base_id WHERE user_uq = ? AND app_uq = ?) AS t1 "
                ."WHERE target_id != 0 AND target_id is not null AND NOT EXISTS (SELECT id FROM app_name WHERE id = target_id AND lang = ?);";
        $stmt = $this->mysqli->prepare($sql);
        error_log(var_export($stmt));
        $stmt->bind_param('sissi',$name,$lang,$user_uq,$app_id,$lang);
        $success = $stmt->execute();
        $stmt->close();
        return $success;
    }
    function getAppLangLink($user_uq,$app_id)
    {
        error_log('<----getAppLangLink---->');
        mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
        $sql = "SELECT an.name,an.lang,lg.iso,lg.name,lg.nativeName FROM app_name AS an LEFT JOIN lang_group AS lg ON an.lang = lg.id WHERE an.id = (select id from app_base_id WHERE user_uq = ? AND app_uq = ?);";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ss',$user_uq,$app_id);
        $stmt->execute();
        //store_resultでバッファのデータを保存
        $stmt->store_result();
        //bind_resultで結果をセットする変数を設定
        $stmt->bind_result($name, $lang, $langIso,$langName,$langNative);
        $this->errorLogging($this->mysqli->error);
        $ary =[];
        while ($stmt->fetch()) {
            array_push($ary,['name'=>$name,'lang'=>$lang,'langIso'=>$langIso,'langName'=>$langName,'langNative'=>$langNative]);
        }
        $stmt->close();
        return $ary;
    }
    function errorLogging($message){
        error_log($message,0);
    }
}
