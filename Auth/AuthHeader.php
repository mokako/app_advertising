<?php
if(!isset($_SESSION)){ 
    session_start(); 
} 
include_once(dirname(__FILE__).'/./Module/AuthClass.php');
include_once(dirname(__FILE__).'/./Module/AuthDataClass.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Auth\Module          as am;

/**
 * Description of AccountView
 *
 * @author moca
 */

class AuthHeade 
{
    //put your code here
    var $ac;
    var $db;
    function __construct()
    {
        $this->ac  =    new am\AuthClass();
        $this->db  =    new am\AuthDataClass('json');
    }
    public function setLogin($parame)
    { 
        if($parame == 0){
            $this->ac->setGoogleAuth_2_0();
        }elseif ($parame == 1){
            //データベースに照合無ければアカウント作成し作成後indexにジャンプ
            $success = $this->db->setAuth($_SESSION['user_email']);
            $this->loadIndex($success);
        }else if($parame == 2){
            $this->setAuthView();
        }else if($parame == 3){
            $_SESSION = array();
            if( ini_get( 'session.use_cookies' ) )
            {
                // セッション クッキーを削除
                $params = session_get_cookie_params();
                setcookie( session_name(), '', time() - 3600, $params[ 'path' ] );
            }
            session_destroy();
            $this->loadIndex();
            exit();
        }
    }
    private function getAuthParam()
    {
        return $this->db->getAuthID($_SESSION['user_email']);
    }
    public function setAuthView() 
    {
        header("Location:./AuthView.php?user_id=".$this->getAuthParam());
        exit();
    }
    public function loadIndex() 
    {
        header("Location:../index.php");
        exit();
    }
}
$login = filter_input(INPUT_GET,'login',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$authView = new AuthHeade();
$authView->setLogin($login);